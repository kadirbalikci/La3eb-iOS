package com.automation.pages;

import com.automation.utilities.Driver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MorePage extends BasePage{

    public MorePage(){

        PageFactory.initElements(new AppiumFieldDecorator(Driver.get()), this);
    }

    @FindBy(id = "TabbarMoreController.SettingsCell.2")
    public MobileElement settings;

    @FindBy(id = "SettingsController.SettingsCell.3")
    public MobileElement changeLanguage;

    @FindBy(name = "تغيير اللغة")
    public MobileElement changeLanguageAr;

    @FindBy(name = "عربي")
    public MobileElement arabicLanguage;

    @FindBy(name = "English")
    public MobileElement englishLanguage;

    @FindBy(name = "Playstation")
    public MobileElement playstationPlatform;

    @FindBy(id = "SurveyViewController.nextButton")
    public MobileElement nextButton;

    @FindBy(name = "Youtube")
    public MobileElement youtubeSocialPlatform;

    @FindBy(name = "PUBG")
    public MobileElement pubGSocialCommunityHub;

    @FindBy(id = "SettingsController.SettingsCell.1")
    public MobileElement profile;

    @FindBy(id = "AboutmeEditCell.textView")
    public MobileElement profileAboutMe;

    @FindBy(id = "ProfileEditController.saveButton")
    public MobileElement profileEditSaveButton;

    @FindBy(id = "TopNotifyView.descriptionLabel")
    public MobileElement topNotify;

    @FindBy(id = "delete")
    public MobileElement deleteBtn;

//    @FindBy(name = "Orders")
//    public MobileElement settingsOrders;
//
//    @FindBy(name = "My orders")
//    public MobileElement
//
//    @FindBy(name = "Wishlist")
//    public MobileElement settingsWishlist;
//
//    @FindBy(name = "My Addresses")
//    public MobileElement settingsMyAddresses;
//
//    @FindBy(name = "Payment options")
//    public MobileElement settingsPaymentOptions;
//
//    @FindBy(name = "Security and Privacy")
//    public MobileElement settingsSecurityAndPrivacy;
//
//    @FindBy(name = "Login Information")
//    public MobileElement settingsLoginInformation;
//
//    @FindBy(name = "My reviews")
//    public MobileElement settingsMyReviews;




}
