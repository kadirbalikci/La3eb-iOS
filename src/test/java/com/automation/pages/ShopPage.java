package com.automation.pages;

import com.automation.utilities.Driver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ShopPage extends BasePage {

    @FindBy(name = "Shop")
    public MobileElement shopButton;

    @FindBy(name = "المتجر")
    public MobileElement shopButtonAr;

    @FindBy(id = "SingleRowProductCell.titleLabel")
    public MobileElement selectProduct;

    @FindBy(id = "ProductCell.titleLabel")
    public MobileElement selectProductSearchResult;

    @FindBy(name = "Wallet Top Up - 5 USD")
    public MobileElement select20USD;

    @FindBy(id = "ProductCTAFooterView.addToCartButton")
    public MobileElement addToCartButton;

    @FindBy(id = "BadgeBarButtonItem.shopBarButton")
    public MobileElement cart;

    @FindBy(id = "CartController.UIBarButtonItem")
    public MobileElement editCart;

    @FindBy(className = "UITableTextAccessibilityElement")
    public MobileElement productsInCart;

    @FindBy(id = "CartItemCell.checkboxButton")
    public MobileElement markFirstProduct;

    @FindBy(name = "Back")
    public MobileElement backToPDP;

    @FindBy(xpath = "//XCUIElementTypeButton[@name=\"Welcome La3eb\"]")
    public MobileElement backSearchBox;

    @FindBy(xpath = "//XCUIElementTypeButton[@name=\"Cancel\"]")
    public MobileElement cancelSearch;

    @FindBy(id = "TabSelectionView.rightButton")
    public MobileElement addToWishlistFromCart;

    @FindBy(id = "TopNotifyView.descriptionLabel")
    public MobileElement topNotifyMessage;

    @FindBy(id = "BadgeBarButtonItem.wishlistBarButton")
    public MobileElement wishlistButton;

    @FindBy(id = "WishlistCell.0")
    public MobileElement wishlistItem;

    @FindBy(id = "TabbarController.Optional(\"Shop\")")
    public MobileElement shop2;

    @FindBy(id = "ProductPriceView.wishlistButton")
    public MobileElement addToWishlistFromPDP;

    @FindBy(id = "PaymentOverlayView.nextStepButton")
    public MobileElement checkout;

    @FindBy(id = "ShippingAddressCell.firstNameTextField")
    public MobileElement firstName;

    @FindBy(id = "ShippingAddressCell.lastNameTextField")
    public MobileElement lastName;

    @FindBy(id = "ShippingAddressCell.emailTextField")
    public MobileElement email;

    @FindBy(id = "PhoneNumberView.phoneNumberTextField")
    public MobileElement phoneNumber;

    @FindBy(id = "ShippingAddressCell.countryTextField")
    public MobileElement country;


    @FindBy(id = "ShippingAddressCell.cityTextField")
    public MobileElement city;

    @FindBy(id = "SearchableItemTableViewCell.0")
    public MobileElement citySelection;

    @FindBy(id = "SearchableItemTableViewCell.0")
    public MobileElement districtSelection;

    @FindBy(id = "ShippingAddressCell.districtTextField")
    public MobileElement district;

    @FindBy(id = "ShippingAddressCell.address1TextField")
    public MobileElement street;

    @FindBy(id = "ShippingController.$__lazy_storage_$_nextStepButton")
    public MobileElement continueToNextStep;

    @FindBy(id = "ShippingController.$__lazy_storage_$_nextStepButton")
    public  MobileElement continueToNext;

    @FindBy(id = "CreditCardInputCell.ownerNameTextField")
    public MobileElement cardHolder;

    @FindBy(id = "CreditCardInputCell.expireDateTextField")
    public MobileElement expDate;

    @FindBy(id = "CreditCardInputCell.cardNumberTextField")
    public MobileElement cardNumber;

    @FindBy(id = "CreditCardInputCell.cvvTextField")
    public MobileElement cvv;

    @FindBy(id = "OrderConfirmView.confirmOrderButton")
    public MobileElement confirmOrder;

    @FindBy(name = "التالي")
    public MobileElement confirmOrderAr;

    @FindBy(id = "DynamicPageController.Optional<UITextField>")
    public MobileElement shopSearchBox;

    @FindBy(name = "plp filter")
    public MobileElement filterButton;

    @FindBy(id = "HeaderView.viewAllButton")
    public MobileElement viewAllButton;

    @FindBy(name = "Games")
    public MobileElement gamesFilter;

    @FindBy(name = "الألعاب")
    public MobileElement gamesFilterAr;

    @FindBy(id = "BlueButton")
    public MobileElement blueButton;

    @FindBy(className = "UICollectionView")
    public MobileElement filterResults;

    @FindBy(className = "UICollectionView")
    public MobileElement searchResults;

    @FindBy(name = "Cash On Delivery")
    public MobileElement codPayment;

    @FindBy(name = "الدفع عند التسليم")
    public MobileElement codPaymentAr;

    @FindBy(name = "NEW CARD")
    public MobileElement ccPayment;

    @FindBy(name = "إضافة codPaymentبطاقة جديدة")
    public MobileElement ccPaymentAr;

    @FindBy(id = "OrderConfirmView.subTotalDescriptionLabel")
    public MobileElement subtotal;

    @FindBy(id = "OrderConfirmView.codTitleLabel")
    public MobileElement codFee;

    @FindBy(id = "GradientImageCell.titleLabel")
    public MobileElement firstPLP;

    @FindBy(id = "HeaderView.titleLabel")
    public MobileElement headerPLP;

    @FindBy(id = "SearchableItemTableViewCell.0")
    public MobileElement countrySelection;

    @FindBy(name = "Games")
    public MobileElement gamesPLPButton;

    @FindBy(name = "الألعاب")
    public MobileElement gamesPLPButtonAr;

    @FindBy(name = "plp filter")
    public MobileElement plpFilterButton;

    @FindBy(name = "Price")
    public MobileElement priceFilter;

    @FindBy(name = "السعر")
    public MobileElement priceFilterAr;

    @FindBy(name = "2")
    public MobileElement minimumAmount; //210   150

    @FindBy(name = "10051")
    public MobileElement maximumAmount;  //210   250

    @FindBy(id = "ProductSummaryView.titleLabel")
    public MobileElement headerPDP;

    @FindBy(name = "Clear text")
    public MobileElement clearSearchBox;

    @FindBy(name = "Wallet Top Up - 20 USD")
    public MobileElement virtualProduct;

    @FindBy(id = "OrderConfirmView.applyDiscountButton")
    public MobileElement applyDiscount;

    @FindBy(id = "PaymentRedeemOptionCell.couponTextField")
    public MobileElement promoCodeTextBox;

    @FindBy(id = "DiscountController.$__lazy_storage_$_bottomButton")
    public MobileElement checkPromoCode;

    @FindBy(id = "PaymentRedeemOptionCell.couponApplyButton")
    public MobileElement applyPromoCode;

    @FindBy(id = "OrderConfirmView.promocodeDescriptionLabel")
    public MobileElement promoDiscount;

    @FindBy(name = "VERIFICATION CODE")
    public MobileElement verificationCodeScreen;

    @FindBy(name = "الدفع")
    public MobileElement verificationCodeScreenAr;

    @FindBy(name = "OrderDetailsCell.orderDetailsButton")
    public MobileElement viewYourOrder;

    @FindBy(name = "Show details")
    public MobileElement showOrderDetails;

    @FindBy(id = "OrderDetailsCell.orderDetailsButton")
    public MobileElement showOrderDetailsAr;

    @FindBy(name = "Payment details")
    public MobileElement paymentDetails;

    @FindBy(name = "تفاصيل أكثر")
    public MobileElement paymentDetailsAr;

    @FindBy(name = "**** 6584")
    public MobileElement paymentMethod;

    @FindBy(id = "Search")
    public MobileElement searchButton;

    @FindBy(id = "البحث")
    public MobileElement searchButtonAr;

    @FindBy(name = "Done")
    public MobileElement doneButton;

    @FindBy(id = "ProductCTAFooterView.buyNowButton")
    public MobileElement buyNowButton;

    @FindBy(name = "God of War™")
    public MobileElement chooseGodOfWarFromPLP;

    @FindBy(name = "PLAYSTATION®5 DIGITAL EDITION")
    public MobileElement playStation5Console;

    @FindBy(name = "PlayStation®5 جهاز")
    public MobileElement playStation5ConsoleAr;

    @FindBy(name = "Shop PlayStation®5")
    public MobileElement backToShopPS5;

    @FindBy(name = "PSN Cards")
    public MobileElement PSNCards;

    @FindBy(name = "PlayStation®5")
    public MobileElement playStation5PLP;

    @FindBy(name = "Sort")
    public MobileElement sortButton;

    @FindBy(name = "فرز")
    public MobileElement sortButtonAr;

    @FindBy(id = "RadioCell.0")
    public MobileElement lowestPrice;

    @FindBy(id = "RadioCell.1")
    public MobileElement highestPrice;

    @FindBy(id = "Lowest Price")
    public MobileElement lowestPriceFiltered;

    @FindBy(name = "أقل سعر")
    public MobileElement lowestPriceFilteredAr;

    @FindBy(id = "Highest Price")
    public MobileElement highestPriceFiltered;

    @FindBy(id = "ProductCell.priceLabel")
    public MobileElement sortedFirstProductPrice;

    @FindBy(id = "ProductSummaryView.ratingsCountLabel")
    public MobileElement reviews;

    @FindBy(id = "StarRatingView.UIButton.4")
    public MobileElement selectionRating;

    @FindBy(id = "SendFeedbackController.submitButton")
    public MobileElement ratingSubmit;

    @FindBy(id = "SendFeedbackController.writeReviewOptionButton")
    public MobileElement writeAReview;

    @FindBy(id = "FeedbackInputCell.fullnameTextField")
    public MobileElement fullNameRating;

    @FindBy(id = "FeedbackInputCell.titleTextField")
    public MobileElement titleRating;

    @FindBy(id = "FeedbackInputCell.descriptionTextField")
    public MobileElement reviewRating;

    @FindBy(name = "FeedbackInputController.submitButton")
    public MobileElement submitRating;

//    @FindBy(id = "FeedbackInputController.submitButton")
//    public MobileElement submitRatingAr;

    @FindBy(id = "TrendingCell.collectionView")
    public MobileElement firstPs5Game;

    @FindBy(id = "TableHeaderView.UIButton")
    public MobileElement manageAddresses;

    @FindBy(name = "NEW SHIPPING ADDRESS")
    public MobileElement newShippingAddress;

    @FindBy(id = "AddressListController.$__lazy_storage_$_newAddressButton")
    public MobileElement addNewAddressButton;

    @FindBy(id = "AddressController.$__lazy_storage_$_doneButton")
    public MobileElement doneAddNewAddress;

    @FindBy(name = "Shipping")
    public MobileElement backFromSavedAddress;

    @FindBy(name = "Payment")
    public MobileElement paymentStepHeader;

    @FindBy(name = "Search")
    public MobileElement backToSearch;

    @FindBy(id = "PlpController.Optional<UITextField>")
    public MobileElement shopSearch;

    @FindBy(name = "Discount (welcome10)")
    public MobileElement welcome10DiscountApplied;

    @FindBy(id = "CartItemCell.checkboxButton")
    public List<WebElement> numberOfProductInCart;

    @FindBy(id = "OrderConfirmView.shippingCostValueLabel")
    public MobileElement shippingAndHandlingFee;

    @FindBy(name = "Your order is successful!")
    public MobileElement orderSuccessful;

    @FindBy(name = "تم تقديم طلبك بنجاح!")
    public MobileElement orderSuccessfulAr;

    @FindBy(id = "OnboardingController.languageOptionsButton")
    public MobileElement languageOptionOnBoarding;





}
