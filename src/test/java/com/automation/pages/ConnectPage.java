package com.automation.pages;

import io.appium.java_client.MobileElement;
import org.openqa.selenium.support.FindBy;

public class ConnectPage extends BasePage{

    @FindBy(id = "GamesHubController.GamesHubFeedCell.1")
    public MobileElement secondChannel;

    @FindBy(id = "GameImageCell.3")
    public MobileElement gameImageConnect;

    @FindBy(name = "channel list")
    public MobileElement channelListButton;

    @FindBy(id = "GamesHubController.Optional<UITextField>")
    public MobileElement searchBoxChannels;

    @FindBy(id = "Search")
    public MobileElement searchButton;

    @FindBy(name = "What's on your mind?")
    public MobileElement messageBoxChannels;

    @FindBy(name = "مالذي تفكر به؟")
    public MobileElement messageBoxChannelsAr;

    @FindBy(id = "NewPostTextCell.textVie")
    public MobileElement textBoxNewPost;

    @FindBy(name = "Playstation® 5")
    public MobileElement ps5ChannelName;

    @FindBy(name = "#stayhome")
    public MobileElement stayhomeChannelName;

    @FindBy(name = "Channels")
    public MobileElement channels;

    @FindBy(name = "القنوات.")
    public MobileElement channelsAr;

    @FindBy(name = "Gamers")
    public MobileElement gamers;

    @FindBy(name = "Minecraft")
    public MobileElement searchedChannel;

    @FindBy(name = "minecraft64")
    public MobileElement searchedGamer;

    @FindBy(id = "GameImageCell.0")
    public MobileElement channelHeader;

    @FindBy(id = "PostShareAccessoryView.uploadButton")
    public MobileElement postShareButton;

    @FindBy(id = "GamerView.userNameLabel")
    public MobileElement lastPostSenderName;

    @FindBy(name = "h")
    public MobileElement h;

    @FindBy(name = "i")
    public MobileElement i;

    @FindBy(name = "hi")
    public MobileElement hiPost;

    @FindBy(name = "hi")
    public MobileElement postText;

    @FindBy(id = "GameFeedPostCell.0")
    public MobileElement firstPost;

    @FindBy(id = "GameFeedPostCell.1")
    public MobileElement secondPost;

    @FindBy(id = "GamerView.userNameLabel")
    public MobileElement userName;

    @FindBy(name = "Repost")
    public MobileElement repost;

    @FindBy(name = "إعادة نشر")
    public MobileElement repostAr;

    @FindBy(id = "TopNotifyView.rightButton")
    public MobileElement topNotify;

    @FindBy(id = "TopNotifyView.descriptionLabel")
    public MobileElement getTopNotifyForRepost;

    @FindBy(name = "WebHasan")
    public MobileElement userForReport;

    @FindBy(id = "GamerView.chevronButton")
    public MobileElement selectionOfPost;

    @FindBy(name = "Report Post")
    public MobileElement reportPostButton;

    @FindBy(name = "الإبلاغ عن المشاركة")
    public MobileElement reportPostButtonAr;

    @FindBy(name = "Spam")
    public MobileElement reasonForReport;

    @FindBy(name = "سبام")
    public MobileElement reasonForReportAr;

    @FindBy(name = "Done")
    public MobileElement doneButton;

    @FindBy(name = "تم")
    public MobileElement doneButtonAr;

    @FindBy(name = "Will be reviewed")
    public MobileElement reportedSuccessfully;

    @FindBy(name = "ستتم مراجعته")
    public MobileElement reportedSuccessfullyAr;

    @FindBy(className = "UITableTextAccessibilityElement")
    public MobileElement post;

    @FindBy(name = "What are your thoughts?")
    public MobileElement commentTextBox;

    @FindBy(name = "ما هو رأيك؟")
    public MobileElement commentTextBoxAr;

    @FindBy(id = "PostShareAccessoryView.uploadButton")
    public MobileElement subCommentShareButton;

    @FindBy(name = "Tester07")
    public MobileElement subCommentHeader;

    @FindBy(name = "PUBG")
    public MobileElement pubGChannel;

    @FindBy(name = "ببجي")
    public MobileElement pubGChannelAr;

    @FindBy(name = "Comment")
    public MobileElement comment;

    @FindBy(name = "minecraft")
    public MobileElement minecraftGamer;

    @FindBy(id = "ProfileInfoCell.followButton")
    public MobileElement followGamerButton;







}
