package com.automation.pages;

import com.automation.utilities.Driver;
import com.automation.utilities.MobileUtilities;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public abstract class BasePage {

    @AndroidFindBy(id = "com.etsy.android:id/btn_link")
    private MobileElement getStarted;

    public BasePage(){
        PageFactory.initElements(new AppiumFieldDecorator(Driver.get()), this);
    }

    Actions actions = new Actions(Driver.get());


    @FindBy(name = "Allow")
    public MobileElement allow;

    @FindBy(id = "TabbarController.Optional(\"Home\")")
    public MobileElement homeButton;

    @FindBy(name = "Login")
    public MobileElement errorLoginButton;

    @FindBy(id = "OnboardingLoginView.continueAsGuestButton")
    public MobileElement continueAsAGuest;

    @FindBy(name = "Skip")
    public MobileElement skipButton;

    @FindBy(name = "تخطي")
    public MobileElement skipButtonAr;

    @FindBy(xpath = "//XCUIElementTypeButton[@name=\"Shop\"]")
    public MobileElement shop;

    @FindBy(xpath = "//XCUIElementTypeButton[@name=\"المتجر\"]")
    public MobileElement shopAr;

    @FindBy(name = "Connect")
    public MobileElement connect;

    @FindBy(name = "القنوات الاجتماعية")
    public MobileElement connectAr;

    @FindBy(id = "HomeController.Optional<UITextField>")
    public MobileElement searchBox;

    @FindBy(id = "OnboardingLoginView.signUpButton")
    public MobileElement signUpButton;

    @FindBy(id = "OnboardingLoginView.loginButton")
    public MobileElement signInButton;

    @FindBy(name = "Welcome to La3eb")
    public MobileElement homeHeader;

    @FindBy(name = "أهلاً بك في La3eb")
    public MobileElement homeHeaderArabic;

    @FindBy(id = "LoginOptionsController.emailButton")
    public MobileElement signInWithEmail;

    @FindBy(id = "LoginController.emailTextField")
    public MobileElement loginEmail;

    @FindBy(id = ".newPassword")
    public MobileElement loginPassword;

    @FindBy(id = "LoginController.loginButton")
    public MobileElement loginButton;

    @FindBy(className = "UITableTextAccessibilityElement")
    public MobileElement homePage;

    @FindBy(id = "FeaturedCell.pageControl")
    public MobileElement homePage2;

    @FindBy(id = "Next")
    public MobileElement nextButton;

    @FindBy(name = "التالي")
    public MobileElement nextButtonAr;

    @FindBy(name = "تم")
    public MobileElement doneButtonAr;

    @FindBy(id = "RegisterController.emailTextField")
    public MobileElement email;

    @FindBy(id = "RegisterController.usernameTextField")
    public MobileElement userName;

    @FindBy(id = ".newPassword")
    public MobileElement password;

    @FindBy(name = "Done")
    public MobileElement doneButton;

    @FindBy(id = "RegisterController.registerButton")
    public MobileElement registerButton;

    @FindBy(id ="RegistrationSummaryController.addPhotoButton")
    public MobileElement addPhoto;

    @FindBy(name = "Open photo library")
    public MobileElement openPhotoLibrary;

    @FindBy(name = "OK")
    public MobileElement okButton;

    @FindBy(name = "Photo, Landscape, August 08, 2012, 7:52 PM")
    public MobileElement selectPhoto;

    @FindBy(id = "RegistrationSummaryController.finishButton")
    public MobileElement finishButton;

    @FindBy(name = "More")
    public MobileElement moreButton;

    @FindBy(name = "المزيد")
    public MobileElement moreButtonAr;

    @FindBy(name = "Clap")
    public MobileElement mySocialFeedClap;

    @FindBy(name = "My Feed")
    public MobileElement myFeed;

    @FindBy(name = "صفحتي")
    public MobileElement myFeedAr;

//    @FindBy(id = "SurveyViewController.skipButton")
//    public MobileElement skipButton;


    public void homeShopBtn(){
//        actions
//        x=125 y=830
    }





    public void clickOnGetStarted() {
        MobileUtilities.waitForPresence(By.id("com.etsy.android:id/btn_link"));
        getStarted.click();
    }
}
