package com.automation.step_definitions;

import com.automation.pages.BasePage;
import com.automation.pages.ConnectPage;
import com.automation.pages.MorePage;
import com.automation.pages.ShopPage;
import com.automation.utilities.ConfigurationReader;
import com.automation.utilities.Driver;
import com.automation.utilities.MobileUtilities;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.interactions.Actions;

public class RegisteredUserArStepDefinitions extends BasePage {

    Actions action = new Actions(Driver.get());
    ConnectPage connectPage = new ConnectPage();
    ShopPage shopPage = new ShopPage();
    MorePage morePage = new MorePage();
    GuestUserArStepDefinitions guestUserArStepDefinitions = new GuestUserArStepDefinitions();

    @Then("user choose Arabic language and log in with email")
    public void user_choose_Arabic_language_and_log_in_with_email() {
        allow.click();
        shopPage.languageOptionOnBoarding.click();
        MobileUtilities.wait(5);
        signInButton.click();
        MobileUtilities.wait(2);
        morePage.loginEmail.click();
        morePage.loginEmail.sendKeys(ConfigurationReader.get("email2"));
        morePage.loginPassword.sendKeys(ConfigurationReader.get("password2"));
        morePage.loginButton.click();
    }

    @Then("verify the user logged in ar")
    public void verify_the_user_logged_in_ar() {

        Assert.assertTrue(homeHeaderArabic.isDisplayed());
    }

    @Given("logged in user should be on home page ar")
    public void logged_in_user_should_be_on_home_page_ar() {
        user_choose_Arabic_language_and_log_in_with_email();
        guestUserArStepDefinitions.user_click_on_skip_ar_for_discover_la3eb();
        guestUserArStepDefinitions.click_on_screen_ar();
    }


    @Then("user clicks on Sign up button ar")
    public void user_clicks_on_Sign_up_button_ar() {
        allow.click();
        shopPage.languageOptionOnBoarding.click();
        MobileUtilities.wait(3);
        signUpButton.click();
        MobileUtilities.wait(4);
    }

    @Then("verify the user should see MyFeed page ar")
    public void verify_the_user_should_see_MyFeed_page_ar() {
        Assert.assertTrue(myFeedAr.isDisplayed());
    }


    @Then("user will add a post ar")
    public void user_will_add_a_post_ar() {
        connectPage.messageBoxChannelsAr.click();
        MobileUtilities.wait(2);
        connectPage.h.click();
        connectPage.i.click();
        connectPage.postShareButton.click();
        MobileUtilities.wait(5);
    }

    @Then("user will put a comment to post ar")
    public void user_will_put_a_comment_to_post_ar() {

        connectPage.channelListButton.click();
        MobileUtilities.wait(1);
        connectPage.pubGChannel.click();
        MobileUtilities.wait(2);

        connectPage.firstPost.click();
        MobileUtilities.wait(5);
        connectPage.commentTextBoxAr.click();
        connectPage.commentTextBoxAr.sendKeys("Sub Comment Test");
        MobileUtilities.wait(2);
        connectPage.subCommentShareButton.click();
        MobileUtilities.wait(3);
    }

    @Then("user will navigates to settings ar")
    public void user_will_navigates_to_settings_ar() {
        moreButtonAr.click();
        morePage.settings.click();
    }

    @Then("user navigates to Profile page ar")
    public void user_navigates_to_Profile_page_ar() {
        moreButtonAr.click();
        morePage.settings.click();
        MobileUtilities.wait(2);
    }

    @Then("user chooses a post for report and report the post ar")
    public void user_chooses_a_post_for_report_and_report_the_post_ar() {
        connectPage.post.click();
        MobileUtilities.wait(1);
        connectPage.selectionOfPost.click();
        connectPage.reportPostButtonAr.click();
        connectPage.reasonForReportAr.click();
        MobileUtilities.wait(3);
    }

    @Then("verify the user reported successfully ar")
    public void verify_the_user_reported_successfully_ar() {
        Assert.assertTrue(connectPage.reportedSuccessfullyAr.isDisplayed());
        connectPage.doneButtonAr.click();
    }

    @Then("user clicks on repost button ar")
    public void user_clicks_on_repost_button_ar() {
        connectPage.repostAr.click();
        MobileUtilities.wait(1);
    }

    @Then("user navigates to shop and PDP ar")
    public void user_navigates_to_shop_and_PDP_ar() {
        shopAr.click();
        MobileUtilities.wait(2);
        shopPage.gamesPLPButtonAr.click();
        MobileUtilities.wait(2);
        shopPage.firstPs5Game.click();
        MobileUtilities.wait(3);
    }

    @Then("user will choose playstation5 ar")
    public void user_will_choose_playstation5_ar() {
        shopAr.click();
        MobileUtilities.wait(2);
        shopPage.playStation5ConsoleAr.click();
        MobileUtilities.wait(1);
    }



}
