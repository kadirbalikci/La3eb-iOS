package com.automation.step_definitions;

import com.automation.pages.BasePage;
import com.automation.pages.ConnectPage;
import com.automation.pages.MorePage;
import com.automation.pages.ShopPage;
import com.automation.utilities.ConfigurationReader;
import com.automation.utilities.Driver;
import com.automation.utilities.MobileUtilities;
import com.github.javafaker.Faker;
import io.cucumber.java.en.*;
import net.bytebuddy.implementation.bytecode.ByteCodeAppender;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class RegisteredUserEnStepDefinitions extends BasePage {

    MorePage morePage = new MorePage();
    ShopPage shopPage = new ShopPage();
    ConnectPage connectPage = new ConnectPage();
    GuestUserEnStepDefinitions guestUserEnStepDefinitions = new GuestUserEnStepDefinitions();
    Actions actions = new Actions(Driver.get());

    Faker faker = new Faker();
    String fakeName = faker.name().firstName() + "07";
    String fakeEmail = fakeName + "@mail.com";

    @Then("user clicks on Sign up button")
    public void user_clicks_on_Sign_up_button() {
        allow.click();
        signUpButton.click();
        MobileUtilities.wait(4);
    }


    @Then("user should logged in with email")
    public void user_should_logged_in_with_email() {
        allow.click();
        signInButton.click();
        MobileUtilities.wait(2);
        morePage.loginEmail.click();
        morePage.loginEmail.sendKeys(ConfigurationReader.get("email2"));
        morePage.loginPassword.sendKeys(ConfigurationReader.get("password2"));
        morePage.loginButton.click();
    }

    @Then("verify the user logged in")
    public void verify_the_user_logged_in() {
        Assert.assertTrue(homeHeader.isDisplayed());
    }


    @Then("user fill credentials")
    public void user_fill_credentials() {


        morePage.email.sendKeys(fakeEmail);
        morePage.userName.click();
        morePage.userName.sendKeys(fakeName);
        morePage.password.sendKeys(ConfigurationReader.get("password3"));

        MobileUtilities.wait(2);
        morePage.registerButton.click();
        MobileUtilities.wait(1);
    }

//    @Then("user clicks on done button")
//    public void user_clicks_on_done_button() {
//        morePage.registerButton.click();
//
//    }

    @Then("user should add photo")
    public void user_should_add_photo() {
        MobileUtilities.wait(3);
        morePage.addPhoto.click();
        morePage.openPhotoLibrary.click();
        morePage.okButton.click();
        MobileUtilities.wait(2);
        morePage.selectPhoto.click();
        MobileUtilities.wait(2);
        morePage.doneButton.click();
        MobileUtilities.wait(2);
        morePage.finishButton.click();
        MobileUtilities.wait(3);

    }

    @Then("user skips survey")
    public void user_skips_survey() {
        morePage.skipButton.click();
        MobileUtilities.wait(2);
        skipButton.click();
        MobileUtilities.wait(2);
    }

    @Then("user will fill tree pages of survey")
    public void user_will_fill_tree_pages_of_survey() {
        morePage.playstationPlatform.click();
        morePage.nextButton.click();
        morePage.youtubeSocialPlatform.click();
        morePage.nextButton.click();
        morePage.pubGSocialCommunityHub.click();
        morePage.nextButton.click();
        MobileUtilities.wait(2);
        skipButton.click();
    }


    @Given("logged in user should be on home page")
    public void logged_in_user_should_be_on_home_page() {
        user_should_logged_in();
        guestUserEnStepDefinitions.user_click_on_skip_for_discover_la3eb();
        guestUserEnStepDefinitions.click_on_screen();
    }


    @Then("verify new account created successfully")
    public void verify_new_account_created_successfully() {
        Assert.assertTrue(homeHeader.isDisplayed());
    }

    @Given("user should logged in")
    public void user_should_logged_in() {
        allow.click();
        signInButton.click();
        MobileUtilities.wait(2);
//        morePage.signInWithEmail.click();
        MobileUtilities.wait(2);
        morePage.loginEmail.click();
        morePage.loginEmail.sendKeys(ConfigurationReader.get("email2"));
        morePage.loginPassword.sendKeys(ConfigurationReader.get("password2"));
        morePage.loginButton.click();
    }

    @Then("user should add multiple product to cart")
    public void user_should_add_multiple_product_to_cart() {
        searchBox.sendKeys(ConfigurationReader.get("sku"));
//        MobileUtilities.wait(2);
        shopPage.searchButton.click();
        MobileUtilities.wait(2);

        shopPage.selectProductSearchResult.click();
        MobileUtilities.wait(2);

        shopPage.addToCartButton.click();
        MobileUtilities.wait(2);


        shopPage.backToSearch.click();
        MobileUtilities.wait(2);

        shopPage.clearSearchBox.click();

        shopPage.shopSearch.sendKeys("esd");
        MobileUtilities.wait(1);


        shopPage.searchButton.click();
        MobileUtilities.wait(2);

        shopPage.selectProductSearchResult.click();
        MobileUtilities.wait(2);

        shopPage.addToCartButton.click();
        MobileUtilities.wait(2);

        shopPage.cart.click();
        MobileUtilities.wait(1);
        shopPage.checkout.click();
        MobileUtilities.wait(2);
    }

    @Then("user navigates to Payment and adds promo code")
    public void user_navigates_to_Payment_and_adds_promo_code() {
        shopPage.continueToNextStep.click();
        MobileUtilities.wait(3);
        shopPage.promoCodeTextBox.sendKeys(ConfigurationReader.get("promoCode"));
        MobileUtilities.wait(4);
        shopPage.applyPromoCode.click();
        MobileUtilities.wait(5);
    }

    @Then("verify promo code applied successfully")
    public void verify_promo_code_applied_successfully() {
        System.out.println(" shopPage.welcome10DiscountApplied.isDisplayed() = " +  shopPage.welcome10DiscountApplied.isDisplayed());
        Assert.assertTrue(shopPage.welcome10DiscountApplied.isDisplayed());

//        System.out.println("shopPage.promoDiscount.isDisplayed() = " + shopPage.promoDiscount.isDisplayed());
//        Assert.assertTrue(shopPage.promoDiscount.isDisplayed());
    }

    @Then("verify the user should see My social feed")
    public void verify_the_user_should_see_My_social_feed() {
//        actions.moveToElement(mySocialFeedClap);
//        System.out.println("mySocialFeedClap.isDisplayed() = " + mySocialFeedClap.isDisplayed());

//        Assert.assertTrue(mySocialFeedClap.isDisplayed());
    }

    @Then("verify the user should see MyFeed page")
    public void verify_the_user_should_see_MyFeed_page() {
        Assert.assertTrue(myFeed.isDisplayed());
    }

    @Then("user will navigates to channel list and choose a channel")
    public void user_will_navigates_to_channel_list_and_choose_a_channel() {
        connectPage.channelListButton.click();
        connectPage.pubGChannel.click();
    }

    @Then("user will choose a channel as a registered")
    public void user_will_choose_a_channel_as_a_registered() {
        connectPage.channelListButton.click();
        MobileUtilities.wait(2);
        connectPage.stayhomeChannelName.click();
    }

    @Then("user will add a post")
    public void user_will_add_a_post() {
        connectPage.messageBoxChannels.click();
        MobileUtilities.wait(2);
        connectPage.h.click();
        connectPage.i.click();
        connectPage.postShareButton.click();
        MobileUtilities.wait(5);
    }

    @Then("verify post is added successfully")
    public void verify_post_is_added_successfully() {
        connectPage.secondPost.click();
        MobileUtilities.wait(2);
        System.out.println("connectPage.userName.getText() = " + connectPage.userName.getText());
        Assert.assertEquals(connectPage.userName.getText(), ConfigurationReader.get("username"));
        Assert.assertTrue(connectPage.postText.isDisplayed());
    }

    @Then("user chooses a post and clicks on post")
    public void user_chooses_a_post_and_clicks_on_post() {
        connectPage.firstPost.click();
        MobileUtilities.wait(5);
    }


    @Then("user claps the post and Unclap the post")
    public void user_claps_the_post_and_Unclap_the_post() {

    }

    @Then("user clicks on repost button")
    public void user_clicks_on_repost_button() {
        connectPage.repost.click();
        MobileUtilities.wait(1);

    }

    @Then("verify reposted successfully")
    public void verify_reposted_successfully() {
        System.out.println("connectPage.getTopNotifyForRepost.isDisplayed() = " + connectPage.getTopNotifyForRepost.isDisplayed());
        Assert.assertTrue(connectPage.getTopNotifyForRepost.isDisplayed());
    }

    @Then("user will click on Buy Now button")
    public void user_will_click_on_Buy_Now_button() {
        shopPage.buyNowButton.click();
        MobileUtilities.wait(4);

    }


    @Then("user chooses a post for report and report the post")
    public void user_chooses_a_post_for_report_and_report_the_post() {
        connectPage.post.click();
        MobileUtilities.wait(1);
        connectPage.selectionOfPost.click();
        connectPage.reportPostButton.click();
        //MobileUtilities.wait(1);
        connectPage.reasonForReport.click();
        MobileUtilities.wait(3);
    }


    @Then("verify the user reported successfully")
    public void verify_the_user_reported_successfully() {
        Assert.assertTrue(connectPage.reportedSuccessfully.isDisplayed());
        connectPage.doneButton.click();
    }

    @Then("user will navigates to channel list and choose another channel")
    public void user_will_navigates_to_channel_list_and_choose_another_channel() {
        connectPage.channelListButton.click();
        MobileUtilities.wait(2);
        connectPage.stayhomeChannelName.click();
    }

    @Then("user navigates to Profile page")
    public void user_navigates_to_Profile_page() {
        moreButton.click();
        morePage.settings.click();
        MobileUtilities.wait(2);
    }

    @Then("user edits profile and saves changes")
    public void user_edits_profile_and_saves_changes() {
        morePage.profile.click();
        MobileUtilities.wait(2);

//        String aboutMe = morePage.profileAboutMe.getText();
//        morePage.profileAboutMe.click();
//        for (int i = 1; i < morePage.profileAboutMe.getText().length(); i++) {
//            morePage.deleteBtn.click();
//        }
//        switch (aboutMe){
//            case "Tester":
//                morePage.profileAboutMe.sendKeys("Testing");
//                break;
//            default:
//                morePage.profileAboutMe.sendKeys("Tester");
//                break;
//        }

        morePage.profileAboutMe.sendKeys("Testing");

        morePage.profileEditSaveButton.click();
        MobileUtilities.wait(1);

        Assert.assertTrue(morePage.topNotify.isDisplayed());
        MobileUtilities.wait(2);
    }

    @Then("user will choose playstation5")
    public void user_will_choose_playstation5() {
        shop.click();
        MobileUtilities.wait(2);
        shopPage.playStation5Console.click();
        MobileUtilities.wait(1);
    }


    @Then("verify the product added wishlist successfully")
    public void verify_the_product_added_wishlist_successfully() {
        MobileUtilities.wait(1);

        if (shopPage.topNotifyMessage.isDisplayed()) {
            Assert.assertTrue(shopPage.topNotifyMessage.isDisplayed());
        } else if (shopPage.topNotifyMessage.isDisplayed() == false) {
            MobileUtilities.wait(2);
            shopPage.addToWishlistFromPDP.click();
            MobileUtilities.wait(1);
            Assert.assertTrue(shopPage.topNotifyMessage.isDisplayed());
        }
    }


    @Then("user will put a comment to post")
    public void user_will_put_a_comment_to_post() {

        connectPage.channelListButton.click();
        MobileUtilities.wait(1);
        connectPage.stayhomeChannelName.click();
        MobileUtilities.wait(2);

        connectPage.firstPost.click();
        MobileUtilities.wait(5);
        connectPage.commentTextBox.click();
        connectPage.commentTextBox.sendKeys("Sub Comment Test");
        MobileUtilities.wait(2);
        connectPage.subCommentShareButton.click();
        MobileUtilities.wait(3);
    }

    @Then("verify the user commented successfully")
    public void verify_the_user_commented_successfully() {
        actions.moveToElement(connectPage.subCommentHeader).build().perform();
        MobileUtilities.wait(1);
        System.out.println("connectPage.subCommentHeader.isDisplayed() = " + connectPage.subCommentHeader.isDisplayed());
        Assert.assertTrue(connectPage.subCommentHeader.isDisplayed());

    }

    @Then("user chooses a user and follow the user")
    public void user_chooses_a_user_and_follow_the_user() {
        connectPage.minecraftGamer.click();
        MobileUtilities.wait(3);

//        MobileUtilities.wait(2);

        actions.moveToElement(connectPage.followGamerButton, 200, 200).click().build().perform();
        MobileUtilities.wait(3);

        System.out.println("connectPage.followGamerButton.getText() = " + connectPage.followGamerButton.getText());

        MobileUtilities.wait(3);
        switch (connectPage.followGamerButton.getText()){
            case "Follow":
                connectPage.followGamerButton.click();
                MobileUtilities.wait(5);
                Assert.assertEquals("Following", connectPage.followGamerButton.getText());
                break;
            case "Following":
                connectPage.followGamerButton.click();
                MobileUtilities.wait(5);
                Assert.assertEquals("Follow", connectPage.followGamerButton.getText());
                break;
        }
    }

    @Then("user will navigates to settings")
    public void user_will_navigates_to_settings() {
        moreButton.click();
        morePage.settings.click();
    }

    @Then("user navigates {string}")
    public void user_navigates(String setting) {
        Driver.get().findElement(By.name(setting)).click();
    }

    @Then("verify the user able to see settings {string}")
    public void verify_the_user_able_to_see_settings(String header) {
        MobileUtilities.wait(3);
        System.out.println("Driver.get().findElement(By.name(header)).isDisplayed() = " + Driver.get().findElement(By.name(header)).isDisplayed());
        Assert.assertTrue(Driver.get().findElement(By.name(header)).isDisplayed());
    }

    @Then("user navigates to shop and PDP")
    public void user_navigates_to_shop_and_PDP() {
        shop.click();
        MobileUtilities.wait(2);
        shopPage.gamesPLPButton.click();
        MobileUtilities.wait(2);
        shopPage.firstPs5Game.click();
        MobileUtilities.wait(3);
    }

    @Then("user sends feedback")
    public void user_sends_feedback() {

        shopPage.reviews.click();
        MobileUtilities.wait(4);
        shopPage.blueButton.click();
        MobileUtilities.wait(1);

        shopPage.selectionRating.click();
        shopPage.ratingSubmit.click();

        shopPage.writeAReview.click();

        shopPage.fullNameRating.sendKeys(ConfigurationReader.get("firstName"));
        shopPage.titleRating.sendKeys("Great");
        shopPage.reviewRating.sendKeys("I recommend it");

        shopPage.doneButton.click();

        shopPage.submitRating.click();

    }

    @Then("verify the user sends rating successfully")
    public void verify_the_user_sends_rating_successfully() {
        MobileUtilities.wait(3);
        Assert.assertTrue(shopPage.topNotifyMessage.isDisplayed());
    }

    @Then("user searches for configurable product and add to cart")
    public void user_searches_for_configurable_product_and_add_to_cart() {
        searchBox.sendKeys(ConfigurationReader.get("confProd"));
        guestUserEnStepDefinitions.user_click_on_view_all_button();
        MobileUtilities.wait(2);
        shopPage.PSNCards.click();
        MobileUtilities.wait(2);
        shopPage.addToCartButton.click();
        MobileUtilities.wait(2);

    }

    @Then("verify product added to cart")
    public void verify_product_added_to_cart() {
        Assert.assertTrue(shopPage.topNotifyMessage.isDisplayed());

    }

    @Then("user will add new shipping details and save the address")
    public void user_will_add_new_shipping_details_and_save_the_address() {
        MobileUtilities.wait(3);
        shopPage.manageAddresses.click();
        MobileUtilities.wait(2);
        shopPage.addNewAddressButton.click();

        MobileUtilities.wait(2);
        shopPage.firstName.sendKeys(ConfigurationReader.get("firstName"));
        shopPage.lastName.sendKeys(ConfigurationReader.get("lastName"));
        shopPage.email.sendKeys(ConfigurationReader.get("email2"));
        shopPage.phoneNumber.sendKeys(ConfigurationReader.get("phoneNumber"));

        connectPage.nextButton.click();
        MobileUtilities.wait(2);
        shopPage.countrySelection.click();
        MobileUtilities.wait(2);
        shopPage.citySelection.click();
        MobileUtilities.wait(2);
        shopPage.districtSelection.click();
        MobileUtilities.wait(2);
        shopPage.street.sendKeys(ConfigurationReader.get("street"));
        shopPage.doneAddNewAddress.click();
        MobileUtilities.wait(5);
        shopPage.backFromSavedAddress.click();
        MobileUtilities.wait(3);
        shopPage.continueToNextStep.click();

    }

    @Then("verify address created successfully")
    public void verify_address_created_successfully() {
        Assert.assertTrue(shopPage.paymentStepHeader.isDisplayed());
    }

    @Given("user cleans cart ar")
    public void user_cleans_cart_ar() {
        shopAr.click();
        shopPage.cart.click();


        shopPage.editCart.click();

//        for (int i = shopPage.numberOfProductInCart.size(); i > 0; i--) {
//
//        }
//
    }

    @Then("verify the user will see shipping and COD fee")
    public void verify_the_user_will_see_shipping_and_COD_fee() {
        MobileUtilities.wait(2);
        Assert.assertTrue(shopPage.codFee.isDisplayed());
        Assert.assertTrue(shopPage.shippingAndHandlingFee.isDisplayed());
    }

    @Then("user clicks on manage and clicks on add new address")
    public void user_clicks_on_manage_and_clicks_on_add_new_address() {
//        shopPage.manageAddresses.click();  //365x190

        shopPage.newShippingAddress.click();

        MobileUtilities.wait(2);

        actions.clickAndHold(shopPage.newShippingAddress)
                .moveByOffset(150,250);
        MobileUtilities.wait(2);

    }

//    @Then("user choose {string} payment method ar")
//    public void user_choose_payment_method_ar(String paymentMethod) {
//        MobileUtilities.wait(5);
//
//        switch (paymentMethod) {
//            case "COD":
//                actions.moveToElement(shopPage.codPayment);
//                MobileUtilities.wait(2);
//                shopPage.codPaymentAr.click();
//                MobileUtilities.wait(2);
//                shopPage.codPaymentAr.click();
//                break;
//            case "CC":
//                shopPage.ccPaymentAr.click();
//                break;
//        }
//        MobileUtilities.wait(4);
//    }




}
