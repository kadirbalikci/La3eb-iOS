package com.automation.step_definitions;

import com.automation.pages.BasePage;
import com.automation.pages.ConnectPage;
import com.automation.pages.MorePage;
import com.automation.pages.ShopPage;
import com.automation.utilities.ConfigurationReader;
import com.automation.utilities.Driver;
import com.automation.utilities.MobileUtilities;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

public class GuestUserArStepDefinitions extends BasePage {

    Actions action = new Actions(Driver.get());
    ConnectPage connectPage = new ConnectPage();
    ShopPage shopPage = new ShopPage();
    MorePage morePage = new MorePage();


    @Given("user should choose Arabic language and continue as a guest")
    public void user_should_choose_Arabic_language_and_continue_as_a_guest() {
        allow.click();
        shopPage.languageOptionOnBoarding.click();
        MobileUtilities.wait(3);
        continueAsAGuest.click();
    }

    @Then("user click on skip ar for discover la3eb")
    public void user_click_on_skip_ar_for_discover_la3eb() {
        skipButtonAr.click();
    }

    @Then("click on screen ar")
    public void click_on_screen_ar() {
        MobileUtilities.wait(4);
//        action.moveByOffset(100,250).click().build();
        action.moveToElement(shopAr, 100, 250).click().build().perform();
        MobileUtilities.wait(3);
    }


    @Then("user navigates back to wishlist from cart ar")
    public void user_navigates_back_to_wishlist_from_cart_ar() {
        MobileUtilities.wait(1);
        Assert.assertTrue(shopPage.topNotifyMessage.isDisplayed());
        shopPage.backToPDP.click();
        shopPage.searchButtonAr.click();
        MobileUtilities.wait(2);
        shopPage.wishlistButton.click();
    }

    @Then("user will navigates to settings and changes language ar")
    public void user_will_navigates_to_settings_and_changes_language_ar() {
        moreButtonAr.click();
        morePage.settings.click();
        morePage.changeLanguageAr.click();
        morePage.englishLanguage.click();
    }

    @Then("verify language changed ar")
    public void verify_language_changed_ar() {
        MobileUtilities.wait(3);
        Assert.assertTrue(homeHeader.isDisplayed());
    }

    @Then("user navigates to shop and clicks on Games ar")
    public void user_navigates_to_shop_and_clicks_on_Games_ar() {
        MobileUtilities.wait(1);
        shopAr.click();
        MobileUtilities.wait(2);
        shopPage.gamesPLPButtonAr.click();
    }

    @Then("user will add price filter ar")
    public void user_will_add_price_filter_ar() {
        shopPage.plpFilterButton.click();
        shopPage.priceFilterAr.click();
        MobileUtilities.wait(1);

        action.moveToElement(shopPage.priceFilterAr,210,150).click().build().perform();
        MobileUtilities.wait(3);
        Driver.get().findElement(By.name("2")).click();
        Driver.get().findElement(By.name("5")).click();
        Driver.get().findElement(By.name("5")).click();
        shopPage.doneButton.click();
        MobileUtilities.wait(2);
        shopPage.blueButton.click();
        MobileUtilities.wait(4);
    }


    @Then("verify the filter applied successfully ar")
    public void verify_the_filter_applied_successfully_ar() {
        Assert.assertTrue(shopPage.searchButtonAr.isDisplayed());
    }

    @Then("user adds category filter ar")
    public void user_adds_category_filter_ar() {
        shopPage.plpFilterButton.click();
        MobileUtilities.wait(3);
        shopPage.gamesPLPButtonAr.click();
        MobileUtilities.wait(2);
        shopPage.blueButton.click();
        MobileUtilities.wait(4);
    }

    @Then("user sorts result ar")
    public void user_sorts_result_ar() {
        shopPage.sortButtonAr.click();
        shopPage.lowestPrice.click();
    }

    @Then("user swaps price filter ar")
    public void user_swaps_price_filter_ar() {
        shopPage.lowestPriceFilteredAr.click();
        shopPage.highestPrice.click();

    }

    @Then("user should add multiple product to cart ar")
    public void user_should_add_multiple_product_to_cart_ar() {
        searchBox.sendKeys(ConfigurationReader.get("sku"));
//        MobileUtilities.wait(2);
        shopPage.searchButton.click();
        MobileUtilities.wait(2);

        shopPage.selectProductSearchResult.click();
        MobileUtilities.wait(2);

        shopPage.addToCartButton.click();
        MobileUtilities.wait(2);


        shopPage.searchButtonAr.click();
        MobileUtilities.wait(2);

        shopPage.clearSearchBox.click();

        shopPage.shopSearch.sendKeys("esd");
        MobileUtilities.wait(1);


        shopPage.searchButtonAr.click();
        MobileUtilities.wait(2);

        shopPage.selectProductSearchResult.click();
        MobileUtilities.wait(2);

        shopPage.addToCartButton.click();
        MobileUtilities.wait(2);

        shopPage.cart.click();
        MobileUtilities.wait(1);
        shopPage.checkout.click();
        MobileUtilities.wait(2);
    }

    @Then("user navigates to Connect ar")
    public void user_navigates_to_Connect_ar() {
        connectAr.click();
        MobileUtilities.wait(2);
    }

    @Then("user will navigates to channel list and choose a channel ar")
    public void user_will_navigates_to_channel_list_and_choose_a_channel_ar() {
        connectPage.channelListButton.click();
        MobileUtilities.wait(1);
        connectPage.pubGChannel.click();
    }

    @Then("verify the user accessed game channel ar")
    public void verify_the_user_accessed_game_channel_ar() {
        Assert.assertTrue(connectPage.messageBoxChannelsAr.isDisplayed());
    }

    @Then("user searches for a channel ar")
    public void user_searches_for_a_channel_ar() {

        action.moveToElement(connectPage.channelsAr).click().build().perform();
        MobileUtilities.wait(2);
    }

    @Then("verify verification code screen is displayed ar")
    public void verify_verification_code_screen_is_displayed_ar() {

        Assert.assertTrue(shopPage.verificationCodeScreenAr.isDisplayed());
    }

    @Then("user navigates PDP for virtual ar")
    public void user_navigates_PDP_for_virtual_ar() {
        shopPage.shopButtonAr.click();
        MobileUtilities.wait(2);
        shopPage.shopSearchBox.sendKeys(ConfigurationReader.get("virtualSku"));
        shopPage.searchButton.click();
        MobileUtilities.wait(3);
        shopPage.select20USD.click();
        MobileUtilities.wait(2);

    }

    @Then("user choose {string} payment method ar")
    public void user_choose_payment_method_ar(String paymentMethod) {
        MobileUtilities.wait(5);

        switch (paymentMethod){
            case "COD":
                shopPage.codPaymentAr.click();
                MobileUtilities.wait(1);
                shopPage.codPaymentAr.click();
                break;
            case "CC":
                shopPage.ccPaymentAr.click();
                break;
        }
        MobileUtilities.wait(4);
    }

    @Then("user fill card details and clicks confirm order ar")
    public void user_fill_card_details_and_clicks_confirm_order_ar() {
        shopPage.cardHolder.sendKeys(ConfigurationReader.get("cardName"));
        shopPage.expDate.sendKeys(ConfigurationReader.get("cardExpiryDate"));
        shopPage.cardNumber.sendKeys(ConfigurationReader.get("cardNumber"));
        shopPage.cvv.sendKeys(ConfigurationReader.get("cardCVV"));

        shopPage.confirmOrderAr.click();

        MobileUtilities.wait(8);
    }

    @Then("user clicks on next button and clicks confirm order ar")
    public void user_clicks_on_next_button_and_clicks_confirm_order_ar() {
        shopPage.blueButton.click();

        MobileUtilities.wait(15);
    }

    @Then("verify the user will check payment details on order history ar")
    public void verify_the_user_will_check_payment_details_on_order_history_ar() {
        shopPage.viewYourOrder.click();
        shopPage.showOrderDetailsAr.click();
        shopPage.paymentDetailsAr.click();
        MobileUtilities.wait(2);
        Assert.assertTrue(shopPage.paymentMethod.isDisplayed());
    }

    @Then("verify the user purchased successful ar")
    public void verify_the_user_purchased_successful_ar() {
        Assert.assertTrue(shopPage.orderSuccessfulAr.isDisplayed());
    }

    @Then("user navigates to shop ar")
    public void user_navigates_to_shop_ar() {
        shopAr.click();
        MobileUtilities.wait(2);
    }
    @Then("user click on filter, choose games and apply filter ar")
    public void user_click_on_filter_choose_games_and_apply_filter_ar() {
        shopPage.filterButton.click();
        MobileUtilities.wait(2);

        shopPage.gamesFilterAr.click();
        MobileUtilities.wait(2);
        shopPage.blueButton.click();

    }

    @Then("user should fill address details and clicks continue ar")
    public void user_should_fill_address_details_and_clicks_continue_ar() {
        shopPage.firstName.sendKeys(ConfigurationReader.get("firstName"));
        shopPage.lastName.sendKeys(ConfigurationReader.get("lastName"));
        shopPage.email.sendKeys(ConfigurationReader.get("email2"));
        shopPage.phoneNumber.sendKeys(ConfigurationReader.get("phoneNumber"));


        connectPage.nextButtonAr.click();
        MobileUtilities.wait(2);
        shopPage.countrySelection.click();
        MobileUtilities.wait(2);
        shopPage.citySelection.click();
        MobileUtilities.wait(2);
        shopPage.districtSelection.click();
        MobileUtilities.wait(2);
        shopPage.street.sendKeys(ConfigurationReader.get("street"));
        MobileUtilities.wait(2);
        shopPage.continueToNextStep.click();

    }



}
