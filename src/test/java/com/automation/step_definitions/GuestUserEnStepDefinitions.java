package com.automation.step_definitions;

import com.automation.pages.BasePage;
import com.automation.pages.ConnectPage;
import com.automation.pages.MorePage;
import com.automation.pages.ShopPage;
import com.automation.utilities.ConfigurationReader;
import com.automation.utilities.Driver;
import com.automation.utilities.MobileUtilities;
import io.cucumber.java.bs.A;
import io.cucumber.java.en.*;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;


public class GuestUserEnStepDefinitions extends BasePage {

    Actions action = new Actions(Driver.get());
    ConnectPage connectPage = new ConnectPage();
    ShopPage shopPage = new ShopPage();
    MorePage morePage = new MorePage();


    String lowestPrice;
    String highestPrice;

    @Given("user should continue as a guest")
    public void user_should_continue_as_a_guest() {
        allow.click();
        continueAsAGuest.click();
    }

    @Then("user click on skip for discover la3eb")
    public void user_click_on_skip_for_discover_la3eb() {
        skipButton.click();
    }

    @Then("click on screen")
    public void click_on_screen() {
        MobileUtilities.wait(3);
//        action.moveByOffset(100,250).click().build();
        action.moveToElement(shop, 100, 250).click().build().perform();
        MobileUtilities.wait(3);
    }

    @Then("user navigates PDP")
    public void user_navigates_PDP() {
        searchBox.sendKeys(ConfigurationReader.get("sku"));
//        MobileUtilities.wait(2);
        shopPage.searchButton.click();
        MobileUtilities.wait(2);

        shopPage.selectProductSearchResult.click();
        MobileUtilities.wait(2);
    }

    @Then("user add product to cart")
    public void user_add_product_to_cart() {
        shopPage.addToCartButton.click();
        MobileUtilities.wait(2);
        shopPage.cart.click();
    }

    @Then("user adds item to wishlist")
    public void user_adds_item_to_wishlist() {
        MobileUtilities.wait(2);
        shopPage.editCart.click();
        shopPage.markFirstProduct.click();
        shopPage.addToWishlistFromCart.click();
    }

    @Then("user navigates back to wishlist from cart")
    public void user_navigates_back_to_wishlist_from_cart() {
        MobileUtilities.wait(1);
        Assert.assertTrue(shopPage.topNotifyMessage.isDisplayed());
        shopPage.backToPDP.click();
        shopPage.searchButton.click();
        MobileUtilities.wait(2);
        shopPage.wishlistButton.click();
    }

    @Then("user navigates to wishlist from pdp")
    public void user_navigates_to_wishlist_from_pdp() {
        Assert.assertTrue(shopPage.topNotifyMessage.isDisplayed());
        shopPage.wishlistButton.click();
    }


    @Then("verify product added to wishlist")
    public void verify_product_added_to_wishlist() {
        Assert.assertTrue(shopPage.wishlistItem.isDisplayed());
    }

    @Then("user click on add to wishlist button")
    public void user_click_on_add_to_wishlist_button() {
        shopPage.addToWishlistFromPDP.click();
    }

    @Then("user navigates back to Connect")
    public void user_navigates_back_to_Connect() {
        connect.click();
    }

    @Then("verify the user should see the channels")
    public void verify_the_user_should_see_the_channels() {
        Assert.assertTrue(connectPage.secondChannel.isDisplayed());
        MobileUtilities.wait(2);
        connectPage.secondChannel.click();
        MobileUtilities.wait(2);
        Assert.assertTrue(connectPage.gameImageConnect.isDisplayed());
    }

    @Then("user navigates to cart and clicks checkout")
    public void user_navigates_to_cart_and_clicks_checkout() {
        shopPage.checkout.click();
    }

    @Then("user should fill address details and clicks continue")
    public void user_should_fill_address_details_and_clicks_continue() {
        shopPage.firstName.sendKeys(ConfigurationReader.get("firstName"));
        shopPage.lastName.sendKeys(ConfigurationReader.get("lastName"));
        shopPage.email.sendKeys(ConfigurationReader.get("email2"));
        shopPage.phoneNumber.sendKeys(ConfigurationReader.get("phoneNumber"));


        connectPage.nextButton.click();
        MobileUtilities.wait(2);
        shopPage.countrySelection.click();
        MobileUtilities.wait(2);
        shopPage.citySelection.click();
        MobileUtilities.wait(2);
        shopPage.districtSelection.click();
        MobileUtilities.wait(2);
        shopPage.street.sendKeys(ConfigurationReader.get("street"));
        MobileUtilities.wait(2);
        shopPage.continueToNextStep.click();

    }

    @Then("user should fill address details and clicks continue for virtual product")
    public void user_should_fill_address_details_and_clicks_continue_for_virtual_product() {
        shopPage.firstName.sendKeys(ConfigurationReader.get("firstName"));
        shopPage.lastName.sendKeys(ConfigurationReader.get("lastName"));
        shopPage.email.sendKeys(ConfigurationReader.get("email2"));
        shopPage.phoneNumber.sendKeys(ConfigurationReader.get("phoneNumber"));
        MobileUtilities.wait(2);
        shopPage.continueToNextStep.click();
    }

    @Then("user fill card details and clicks confirm order")
    public void user_fill_card_details_and_clicks_confirm_order() {
        shopPage.cardHolder.sendKeys(ConfigurationReader.get("cardName"));
        shopPage.expDate.sendKeys(ConfigurationReader.get("cardExpiryDate"));
        shopPage.cardNumber.sendKeys(ConfigurationReader.get("cardNumber"));
        shopPage.cvv.sendKeys(ConfigurationReader.get("cardCVV"));

        shopPage.confirmOrder.click();

        MobileUtilities.wait(8);
    }


    @Then("user navigates to shop")
    public void user_navigates_to_shop() {
        shop.click();
        MobileUtilities.wait(2);
    }

    @Then("user click on filter, choose games and apply filter")
    public void user_click_on_filter_choose_games_and_apply_filter() {
        shopPage.filterButton.click();
        MobileUtilities.wait(2);

        shopPage.gamesFilter.click();
//        action.moveByOffset(200,450).build();
//        action.click();

        //action.moveByOffset( 200, 450).click().build().perform();
        MobileUtilities.wait(2);
        shopPage.blueButton.click();

    }

    @Then("verify the user filtered games")
    public void verify_the_user_filtered_games() {
        MobileUtilities.wait(2);
        Assert.assertTrue(shopPage.filterResults.isDisplayed());

    }

    @Then("user searches game")
    public void user_searches_game() {
        shopPage.shopSearchBox.sendKeys(ConfigurationReader.get("gameName"));
        MobileUtilities.wait(2);

    }

    @Then("user click on view all button")
    public void user_click_on_view_all_button() {
        shopPage.viewAllButton.click();
        MobileUtilities.wait(2);
    }

    @Then("verify the user should see the game")
    public void verify_the_user_should_see_the_game() {
        Assert.assertTrue(shopPage.searchResults.isDisplayed());
    }

    @Then("user choose {string} payment method")
    public void user_choose_payment_method_and_clicks_continue(String paymentMethod) {
        MobileUtilities.wait(5);

        switch (paymentMethod) {
            case "COD":
                action.moveToElement(shopPage.codPayment);
                MobileUtilities.wait(2);
                shopPage.codPayment.click();
                MobileUtilities.wait(2);
                shopPage.codPayment.click();
                break;
            case "CC":
                shopPage.ccPayment.click();
                break;
        }
        MobileUtilities.wait(4);
    }

    @Then("user clicks continue to next step")
    public void user_clicks_continue_to_next_step() {
        MobileUtilities.wait(2);
        shopPage.continueToNext.click();
    }

    @Then("user clicks on next button")
    public void user_clicks_on_next_button() {
        shopPage.nextButton.click();

    }

    @Then("verify the user should see shopping fee and COD fee")
    public void verify_the_user_should_see_shopping_fee_and_COD_fee() {
        MobileUtilities.wait(1);
        Assert.assertTrue(shopPage.subtotal.isDisplayed());
        Assert.assertTrue(shopPage.subtotal.isDisplayed());
    }

    @Then("user should choose sub category for PLP")
    public void user_should_choose_sub_category_for_PLP() {
        shopPage.firstPLP.click();

    }

    @Then("verify the user on PLP")
    public void verify_the_user_on_PLP() {
        MobileUtilities.wait(2);
        Assert.assertTrue(shopPage.headerPLP.isDisplayed());
    }

    @Then("verify the user proceed as a guest user")
    public void verify_the_user_proceed_as_a_guest_user() {
        MobileUtilities.wait(5);
        System.out.println("shopPage.homePage.isDisplayed() = " + shopPage.homePage.isDisplayed());
//        System.out.println("shopPage.homePage2.isDisplayed() = " + shopPage.homePage2.isDisplayed());
        System.out.println("searchBox = " + searchBox.isDisplayed());
        //Assert.assertTrue(shopPage.homePage.isDisplayed());
    }

    @Then("user will navigates to settings and changes language")
    public void user_will_navigates_to_settings_and_changes_language() {
        moreButton.click();
        morePage.settings.click();
        morePage.changeLanguage.click();
        morePage.arabicLanguage.click();
    }

    @Then("verify language changed")
    public void verify_language_changed() {
        MobileUtilities.wait(3);
        Assert.assertTrue(homeHeaderArabic.isDisplayed());
    }

    @Then("user navigates to shop and clicks on Games")
    public void user_navigates_to_shop_and_clicks_on_Games() {
        MobileUtilities.wait(1);
        shop.click();
        MobileUtilities.wait(2);
        shopPage.gamesPLPButton.click();
    }

    @Then("user will add price filter")
    public void user_will_add_price_filter() {
        shopPage.plpFilterButton.click();
        shopPage.priceFilter.click();
        MobileUtilities.wait(1);

        action.moveToElement(shopPage.priceFilter, 210, 150).click().build().perform();
        MobileUtilities.wait(3);
        Driver.get().findElement(By.name("2")).click();
        Driver.get().findElement(By.name("5")).click();
        Driver.get().findElement(By.name("5")).click();
        shopPage.doneButton.click();
        MobileUtilities.wait(2);


//        action.moveToElement(shopPage.priceFilter,220,250).click().build().perform();
//        MobileUtilities.wait(3);
//        Driver.get().findElement(By.name("2")).click();
//        Driver.get().findElement(By.name("5")).click();
//        Driver.get().findElement(By.name("0")).click();

//        MobileUtilities.wait(2);
//        shopPage.doneButton.click();
        shopPage.blueButton.click();
        MobileUtilities.wait(4);
    }

    @Then("verify the filter applied successfully")
    public void verify_the_filter_applied_successfully() {
        Assert.assertTrue(shopPage.searchButton.isDisplayed());
    }

    @Then("verify the user on PDP")
    public void verify_the_user_on_PDP() {
        MobileUtilities.wait(2);
        Assert.assertTrue(shopPage.headerPDP.isDisplayed());
    }

    @Then("user navigates to Connect")
    public void user_navigates_to_Connect() {
        connect.click();
        MobileUtilities.wait(2);
    }

    @Then("verify the user on connect page")
    public void verify_the_user_on_connect_page() {

        Assert.assertTrue(connectPage.channelListButton.isDisplayed());
    }

    @Then("user will choose a channel")
    public void user_will_choose_a_channel() {
        MobileUtilities.wait(1);
        connectPage.ps5ChannelName.click();
        MobileUtilities.wait(2);
    }

    @Then("verify the user accessed game channel")
    public void verify_the_user_accessed_game_channel() {
        Assert.assertTrue(connectPage.messageBoxChannels.isDisplayed());
    }

    @Then("user will confirm order")
    public void user_will_confirm_order() {
        shopPage.confirmOrder.click();
        MobileUtilities.wait(3);
    }

    @Then("user fill card details")
    public void user_fill_card_details() {
        shopPage.cardHolder.sendKeys(ConfigurationReader.get("cardName"));
        shopPage.expDate.sendKeys(ConfigurationReader.get("cardExpiryDate"));
        shopPage.cardNumber.sendKeys(ConfigurationReader.get("cardNumber"));
        shopPage.cvv.sendKeys(ConfigurationReader.get("cardCVV"));

    }

    @Then("user clicks on next button and clicks confirm order")
    public void user_clicks_on_next_button_and_clicks_confirm_order() {

        shopPage.nextButton.click();
        MobileUtilities.wait(2);
        shopPage.blueButton.click();

        MobileUtilities.wait(15);
    }

    @Then("verify the user purchased successful")
    public void verify_the_user_purchased_successful() {
        Assert.assertTrue(shopPage.orderSuccessful.isDisplayed());
    }

    @Then("verify verification code screen is displayed")
    public void verify_verification_code_screen_is_displayed() {

        Assert.assertTrue(shopPage.verificationCodeScreen.isDisplayed());
    }

    @Then("user navigates PDP for virtual")
    public void user_navigates_PDP_for_virtual() {
        shopPage.shopButton.click();
        MobileUtilities.wait(2);
        shopPage.shopSearchBox.sendKeys(ConfigurationReader.get("virtualSku"));
        shopPage.searchButton.click();
        MobileUtilities.wait(3);
        shopPage.select20USD.click();
        MobileUtilities.wait(2);

    }

    @Then("verify the user will check payment details on order history")
    public void verify_the_user_will_check_payment_details_on_order_history() {
        shopPage.viewYourOrder.click();
        shopPage.showOrderDetails.click();
        shopPage.paymentDetails.click();
        MobileUtilities.wait(2);
        Assert.assertTrue(shopPage.paymentMethod.isDisplayed());
    }

    @Then("user searches for a channel")
    public void user_searches_for_a_channel() {

        action.moveToElement(connectPage.channels).click().build().perform();
        MobileUtilities.wait(2);

    }

    @Then("verify the user able to search channels")
    public void verify_the_user_able_to_search_channels() {
        Assert.assertTrue(connectPage.searchedChannel.isDisplayed());
        MobileUtilities.wait(2);
    }

    @Then("user searches for a gamer")
    public void user_searches_for_a_gamer() {
        connectPage.channelListButton.click();
        MobileUtilities.wait(1);

        connectPage.searchBoxChannels.sendKeys(ConfigurationReader.get("gamerName"));
        MobileUtilities.wait(2);
        connectPage.searchButton.click();
    }

    @Then("verify the user able to search gamers")
    public void verify_the_user_able_to_search_gamers() {
        Assert.assertTrue(connectPage.searchedGamer.isDisplayed());
        MobileUtilities.wait(2);
    }

    @Then("user adds category filter")
    public void user_adds_category_filter() {
        shopPage.plpFilterButton.click();
        MobileUtilities.wait(3);
        shopPage.gamesFilter.click();
        MobileUtilities.wait(2);
        shopPage.blueButton.click();
        MobileUtilities.wait(4);

    }

    @Then("user sorts result")
    public void user_sorts_result() {
        shopPage.sortButton.click();
        shopPage.lowestPrice.click();

    }

    @Then("verify the user sorted Lowest Price successfully")
    public void verify_the_user_sorted_Lowest_Price_successfully() {
        MobileUtilities.wait(2);
        lowestPrice = shopPage.sortedFirstProductPrice.getText();

    }

    @Then("user swaps price filter")
    public void user_swaps_price_filter() {
        shopPage.lowestPriceFiltered.click();
        shopPage.highestPrice.click();

    }

    @Then("verify the user sorted Highest Price successfully")
    public void verify_the_user_sorted_Highest_Price_successfully() {
        MobileUtilities.wait(2);
        highestPrice = shopPage.sortedFirstProductPrice.getText();

    }

    @Then("verify sorted lowest and highest price are different")
    public void verify_sorted_lowest_and_highest_price_are_different() {
        Assert.assertNotEquals(lowestPrice, highestPrice);
    }

}
