@guestEn
Feature: English Language Guest user tests
  As a guest User 18

  Background:
    Given user should continue as a guest
    Then user click on skip for discover la3eb
    And click on screen


  Scenario: Guest can add items to wishlist from the cart
    Then user navigates PDP
    And user add product to cart
    Then user adds item to wishlist
    Then user navigates back to wishlist from cart
    And verify product added to wishlist


  Scenario: Guest can add items to wishlist from the PDP
    Then user navigates PDP
    And user click on add to wishlist button
    Then user navigates to wishlist from pdp
    And verify product added to wishlist


  Scenario: Guest can change the language from settings
    Then user will navigates to settings and changes language
    And verify language changed


  Scenario: Guest can price filtration on Games
    Then user navigates to shop and clicks on Games
    Then user will add price filter
    And verify the filter applied successfully


  Scenario: Guest can sort games
    Then user navigates to shop and clicks on Games
    And user adds category filter
    Then user sorts result
    And verify the user sorted Lowest Price successfully
    Then user swaps price filter
    And verify the user sorted Highest Price successfully
    Then verify sorted lowest and highest price are different


  Scenario: Guest user can visit PDP
    Then user navigates PDP
    And verify the user on PDP

#  apply promo code does not work
  Scenario: Guest user adds multiple products to cart and apply promo code
    Then user should add multiple product to cart
    And user should fill address details and clicks continue
    And  user navigates to Payment and adds promo code
    Then verify promo code applied successfully


  Scenario: Guest user can access Connect
    Then user navigates to Connect
    And verify the user on connect page

#  @wip
  Scenario: Guest user can access game channels
    Then user navigates to Connect
    And user will navigates to channel list and choose a channel
    Then verify the user accessed game channel
#    And user will choose a channel

  Scenario: Guest can search for other users and channels
    Then user navigates to Connect
    And user searches for a gamer
    Then verify the user able to search gamers
    And user searches for a channel
    Then verify the user able to search channels



  Scenario: Guest user can enter billing details and purchase the order using COD
    Then user navigates PDP
    And user add product to cart
    Then user navigates to cart and clicks checkout
    And user should fill address details and clicks continue
    Then user choose "COD" payment method
    And user will confirm order
    Then verify verification code screen is displayed


  Scenario: Guest user can purchase a virtual product using "Credit Cart" payment method, and check the payment details on order history
    Then user navigates PDP for virtual
    And user add product to cart
    Then user navigates to cart and clicks checkout
    And user should fill address details and clicks continue for virtual product
    Then user choose "CC" payment method
    And user fill card details and clicks confirm order
    Then user clicks on next button and clicks confirm order
    Then verify the user will check payment details on order history

#  @wip
  Scenario: guest user can enter billing address to purchase product
    Then user navigates PDP
    And user add product to cart
    Then user navigates to cart and clicks checkout
    And user should fill address details and clicks continue
    Then user choose "CC" payment method
    And user fill card details and clicks confirm order
    Then user clicks on next button and clicks confirm order
    And verify the user purchased successful



  Scenario: Guest user can filter Games
    Then user navigates to shop
    Then user click on filter, choose games and apply filter
    And verify the user filtered games


  Scenario: Guest user can filter a Game
    Then user navigates to shop
    And user searches game
    Then user click on view all button
    And verify the user should see the game


  Scenario: Guest user can see shipping fee and COD fee
    Then user navigates PDP
    And user add product to cart
    Then user navigates to cart and clicks checkout
    And user should fill address details and clicks continue
    Then user choose "COD" payment method
    And verify the user should see shopping fee and COD fee


  Scenario: Guest user can visit PLP
    Then user navigates to shop
    And user should choose sub category for PLP
    Then verify the user on PLP


  Scenario: Guest user can proceed as a Guest User
    Then verify the user proceed as a guest user

#  T60128   Guest can add items to wishlist from the cart	 +
#  T60127	Guest can add items to wishlist from the PDP	 +
#  T60132	Guest can change the language from settings
#  T60123	Guest can filter games
#  T60133	Guest can search for other users and channels
#  T60124	Guest can sort games
#  T60125	Guest can visit PDP
#  T60121	Guest can visit PLP
#  T60126	Guest user adds multiple products to cart and apply promo code
#  T60119	Guest user can access "Game Hub"
#  T60120	Guest user can access game channels
#  T60129	Guest user can enter billing details and purchase the order using COD
#  T60130	Guest user can purchase a virtual product using "Credit Cart" payment method, and check the payment details on order history
#  T60131	Guest user can see shipping fee and cash on delivery fee
#  T60122	Guest user visits shop games category and search for games
#  T60118	Proceed as Guest user

