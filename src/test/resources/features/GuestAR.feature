@guestAr
Feature: Arabic Language Guest user tests
  As a guest User 18

  Background:
    Given user should choose Arabic language and continue as a guest
    Then user click on skip ar for discover la3eb
    And click on screen ar


  Scenario: Guest can add items to wishlist from the cart
    Then user navigates PDP
    And user add product to cart
    Then user adds item to wishlist
    Then user navigates back to wishlist from cart ar
    And verify product added to wishlist


  Scenario: Guest can add items to wishlist from the PDP
    Then user navigates PDP
    And user click on add to wishlist button
    Then user navigates to wishlist from pdp
    And verify product added to wishlist


  Scenario: Guest can change the language from settings
    Then user will navigates to settings and changes language ar
    And verify language changed ar


  Scenario: Guest can price filtration on Games
    Then user navigates to shop and clicks on Games ar
    Then user will add price filter ar
    And verify the filter applied successfully ar


  Scenario: Guest can sort games
    Then user navigates to shop and clicks on Games ar
    And user adds category filter ar
    Then user sorts result ar
    And verify the user sorted Lowest Price successfully
    Then user swaps price filter ar
    And verify the user sorted Highest Price successfully
    Then verify sorted lowest and highest price are different


  Scenario: Guest user can visit PDP
    Then user navigates PDP
    And verify the user on PDP

#  apply promo code does not work

  Scenario: Guest user adds multiple products to cart and apply promo code
    Then user should add multiple product to cart ar
    And user should fill address details and clicks continue ar
    And  user navigates to Payment and adds promo code
    Then verify promo code applied successfully


  Scenario: Guest user can access Connect
    Then user navigates to Connect ar
    And verify the user on connect page


  Scenario: Guest user can access game channels
    Then user navigates to Connect ar
    And user will navigates to channel list and choose a channel
    Then verify the user accessed game channel ar


  Scenario: Guest can search for other users and channels
    Then user navigates to Connect ar
    And user searches for a gamer
    Then verify the user able to search gamers
    And user searches for a channel ar
    Then verify the user able to search channels


  Scenario: Guest user can enter billing details and purchase the order using COD
    Then user navigates PDP
    And user add product to cart
    Then user navigates to cart and clicks checkout
    And user should fill address details and clicks continue ar
    Then user choose "COD" payment method
    And user will confirm order
    Then verify verification code screen is displayed ar


  Scenario: Guest user can purchase a virtual product using "Credit Cart" payment method, and check the payment details on order history
    Then user navigates PDP for virtual ar
    And user add product to cart
    Then user navigates to cart and clicks checkout
    And user should fill address details and clicks continue for virtual product
    Then user choose "CC" payment method ar
    And user fill card details and clicks confirm order ar
    Then user clicks on next button and clicks confirm order ar
    Then verify the user will check payment details on order history ar


  Scenario: guest user can enter billing address to purchase product
    Then user navigates PDP
    And user add product to cart
    Then user navigates to cart and clicks checkout
    And user should fill address details and clicks continue ar
    Then user choose "CC" payment method ar
    And user fill card details and clicks confirm order ar
    Then user clicks on next button and clicks confirm order ar
    And verify the user purchased successful ar



  Scenario: Guest user can filter Games
    Then user navigates to shop ar
    Then user click on filter, choose games and apply filter ar
    And verify the user filtered games


  Scenario: Guest user can filter a Game
    Then user navigates to shop ar
    And user searches game
    Then user click on view all button
    And verify the user should see the game

#  @wip
  Scenario: Guest user can see shipping fee and COD fee
    Then user navigates PDP
    And user add product to cart
    Then user navigates to cart and clicks checkout
    And user should fill address details and clicks continue ar
    Then user choose "COD" payment method ar
    And verify the user should see shopping fee and COD fee


  Scenario: Guest user can visit PLP
    Then user navigates to shop ar
    And user should choose sub category for PLP
    Then verify the user on PLP


  Scenario: Guest user can proceed as a Guest User
    Then verify the user proceed as a guest user