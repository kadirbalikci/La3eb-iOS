@registeredAr
Feature: Registered user Arabic Rests
  As a Registered User


  Scenario: User login with email
    Given logged in user should be on home page ar
    And verify the user logged in ar


#    Enter a valid email
  Scenario: User creates an account using email
    Then user clicks on Sign up button ar
    And user fill credentials
    Then user should add photo
    And user skips survey
    Then click on screen ar
    Then verify new account created successfully

#    Enter a valid email
  Scenario: Survey can be filled by the newly registered user
    Then user clicks on Sign up button ar
    And user fill credentials
    Then user should add photo
    Then user will fill tree pages of survey
    And click on screen
    Then verify new account created successfully


  Scenario: Registered user can see my feed on home page and social
    Given logged in user should be on home page ar
    And user navigates to Connect ar
    Then verify the user should see MyFeed page ar


  Scenario: Registered user can follow channels and unfollow another channel
    Given logged in user should be on home page ar
    Then user navigates to Connect ar
    And user will navigates to channel list and choose a channel ar


  Scenario: Registered user can add a new post
    Given logged in user should be on home page ar
    Then user navigates to Connect ar
    And user will navigates to channel list and choose a channel ar
    Then user will add a post ar
    And  verify post is added successfully


  Scenario: Registered user can put a comment for a post
    Given logged in user should be on home page ar
    Then user navigates to Connect ar
    Then user will put a comment to post ar
    And verify the user commented successfully


  Scenario: Registered user can follow the other users > unfollow another user
    Given logged in user should be on home page ar
    Then user navigates to Connect ar
    And user searches for a gamer
    Then user chooses a user and follow the user


  Scenario Outline: User can check the account "<settings>"
    Given logged in user should be on home page ar
    Then user will navigates to settings ar
    And user navigates "<settings>"
    Then verify the user able to see settings "<headers>"
    Examples:
      | settings             | headers              |
      | الطلبات              | طلباتي               |
      | قائمة المفضلة        | قائمة المفضلة        |
      | عنواني               | العناوين المحفوظة    |
      | خيارات الدفع         | البطاقات المحفوظة    |
      | الأمن وتسجيل الدخول   | الأمن وتسجيل الدخول   |
      | معلومات تسجيل الدخول | معلومات تسجيل الدخول |
      | مراجعاتي             | مراجعاتي             |


  Scenario: User can edit the profile
    Given logged in user should be on home page ar
    Then user navigates to Profile page ar
    And user edits profile and saves changes


  Scenario: User can change the language from setting
    Given logged in user should be on home page ar
    Then user will navigates to settings and changes language ar
    And verify language changed ar

#Need to check! assertion for clap?
  Scenario: Logged user can like posts and dislike another post
    Given logged in user should be on home page ar
    Then user navigates to Connect ar
    And user will navigates to channel list and choose a channel ar
#    Then user chooses a post and clicks on post
#    And user claps the post and Unclap the post


  Scenario: Logged user can report posts
    Given logged in user should be on home page ar
    Then user navigates to Connect ar
    And user will navigates to channel list and choose a channel ar
    Then user chooses a post for report and report the post ar
    And verify the user reported successfully ar


  Scenario: Logged user can repost
    Given logged in user should be on home page ar
    Then user navigates to Connect ar
    And user will navigates to channel list and choose a channel ar
    Then user chooses a post and clicks on post
    And user clicks on repost button ar
    Then verify reposted successfully


  Scenario: User can visit PLP
    Given logged in user should be on home page ar
    Then user navigates to shop ar
    And user should choose sub category for PLP
    Then verify the user on PLP


  Scenario: User visits shop games category and search for games
    Given logged in user should be on home page ar
    Then user navigates to shop ar
    And user searches game
    Then user click on view all button
    And verify the user should see the game


  Scenario: User filters games and price
    Given logged in user should be on home page ar
    Then user navigates to shop and clicks on Games ar
    Then user will add price filter ar
    And verify the filter applied successfully ar


  Scenario: User can sort games
    Given logged in user should be on home page ar
    Then user navigates to shop and clicks on Games ar
    And user adds category filter ar
    Then user sorts result ar
    And verify the user sorted Lowest Price successfully
    Then user swaps price filter ar
    And verify the user sorted Highest Price successfully
    Then verify sorted lowest and highest price are different


  Scenario: User can visit PDP
    Given logged in user should be on home page ar
    Then user navigates PDP
    And verify the user on PDP


  Scenario: User can writes a product rating
    Given logged in user should be on home page ar
    Then user navigates to shop and PDP ar
    And user sends feedback
    Then verify the user sends rating successfully


  Scenario: User adds configurable product to cart
    Given logged in user should be on home page ar
    Then user searches for configurable product and add to cart
    And verify product added to cart

#  @wip #apply button
  Scenario: User add multiple products to cart and apply promo code
    Given logged in user should be on home page ar
    Then user should add multiple product to cart ar
    And user navigates to Payment and adds promo code
    Then verify promo code applied successfully


  Scenario: User adding items to wishlist from the PDP
    Given logged in user should be on home page ar
    Then user navigates to shop ar
    And user will choose playstation5 ar
    And user click on add to wishlist button
    Then verify the product added wishlist successfully


  Scenario: User adding items to wishlist from the cart
    Given logged in user should be on home page ar
    Then user navigates PDP
    And user add product to cart
    Then user adds item to wishlist
    Then verify the product added wishlist successfully

#  @wip not clicking to manage
  Scenario: Registered user can enter shipping details, save the address
    Given logged in user should be on home page ar
    Then user navigates PDP
    And user add product to cart
    Then user navigates to cart and clicks checkout
    And user will add new shipping details and save the address
    Then verify address created successfully


  Scenario: Registered user can enter billing details and purchase the order using COD
    Given logged in user should be on home page ar
    Then user navigates PDP
    And user add product to cart
    Then user navigates to cart and clicks checkout
    And user clicks continue to next step
    Then user choose "COD" payment method ar
    And user will confirm order
    Then verify verification code screen is displayed ar

#  Wallet
  Scenario: Registered user access shipping details through 'Buy now' button and purchse a simple product using "Wallet" payment method
    Given logged in user should be on home page
    Then user navigates PDP
    And user will click on Buy Now button
    And user clicks continue to next step

#    Onboarding > Login using email > Click on "Shop" > Select simple product > Go to PDP > Click on 'Buy Now' > Proceed to checkout > Enter shipping/billing address > Click on "Continue next step" > redirect to payment section > Select "Wallet" > Place order


  Scenario: User can see shipping fee and cash on delivery fee
    Given logged in user should be on home page ar
#    And user cleans cart ar
    Then user navigates PDP
    And user add product to cart
    Then user navigates to cart and clicks checkout
    And user clicks continue to next step
    Then user choose "COD" payment method ar
    And verify the user will see shipping and COD fee


  Scenario: User can search for other users and channels
    Given logged in user should be on home page ar
    Then user navigates to Connect ar
    And user searches for a gamer
    Then verify the user able to search gamers
    And user searches for a channel ar
    Then verify the user able to search channels



#  @wip  #  Manage address click problem
  @wip
  Scenario: Registered user can enter billing details and purchase the order using COD
    Given logged in user should be on home page ar
    Then user navigates PDP
    And user add product to cart
    Then user navigates to cart and clicks checkout
    And user clicks on manage and clicks on add new address
    Then user should fill address details and clicks continue ar
    And user choose "COD" payment method ar
    And user will confirm order
    Then verify verification code screen is displayed ar

